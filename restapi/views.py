from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework.decorators import api_view, throttle_classes, permission_classes
from rest_framework.response import Response
from .throttle import ReadRateThrottle, WriteRateThrottle
from .models import Task
from .serializers import TaskSerializer
from rest_framework.views import APIView


class TaskHandler(APIView):
    serializer_class = TaskSerializer
    throttle_classes = (WriteRateThrottle,)
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def reorderResults(task):
        try:
            return task.reorderResults
        except:
            return None

    @staticmethod
    def tags(task):
        if task.tags_cache is not None:
            return task.tags_cache
        else:
            return ""

    def _reorderTask(self, task, data, project):
        # PLEASE REMEMBER - both this and the story order handler can reorder stories, so modify both if you modify one.
        modified = []
        before_id = data["task_before_id"]
        after_id = data["task_after_id"]
        modifiedOtherTasks, modified = reorderTask(before_id, after_id, task)
        return {'modifiedOther': modifiedOtherTasks, 'newOrder': task.order, 'storiesModified': modified}

    def _task_assignee(self, task):
        if task.assignee:
            return {
                "first_name": task.assignee.first_name,
                "last_name": task.assignee.last_name,
                "username": task.assignee.username,
                "id": task.assignee.id
            }
        else:
            return None

    def get(self, request, organization_slug, project_slug, story_id=None, iteration_id=None, task_id=None):
        TaskHandler.throttle_classes = (ReadRateThrottle,)
        org, project = checkOrgProject(request, organization_slug, project_slug)
        if story_id is not None:
            story = project.stories.get(id=story_id)
            if task_id is None:
                return story.tasks.all().select_related('assignee').order_by("status", "order")
            else:
                return story.tasks.get(id=task_id)

        elif iteration_id is not None:
            iteration = project.iterations.get(id=iteration_id)
            return Task.objects.filter(story__iteration=iteration).select_related('assignee')

        else:
            by_assignees = request.GET.get("by_assignees", "false") == "true"
            assignees = request.GET.get("assignee", None)

            iterations = project_manager.allCurrentIterations(project)
            tasks = Task.objects.filter(
                story__iteration__in=iterations,
                assignee__isnull=False
            ).select_related('assignee')

            if not by_assignees:
                return tasks
            else:
                r_tasks = []
                for assignee in project.assignable_members():
                    if assignees is not None and str(assignee.id) not in assignees.split(","):
                        continue

                    r_tasks.append({
                        "user": {
                            "id": assignee.id,
                            "email": assignee.email,
                            "username": assignee.username,
                            "first_name": assignee.first_name,
                            "last_name": assignee.last_name,
                        },
                        "tasks": tasks.filter(assignee=assignee)
                    })
                return r_tasks

    def post(self, request, organization_slug, project_slug, story_id, iteration_id=None):
        data = request.data

        org, project = checkOrgProject(request, organization_slug, project_slug, True)

        story = project.stories.get(id=story_id)

        task = Task(story=story)
        for field in TaskHandler.write_fields:
            if field in data:
                setattr(task, field, data[field])

        if "status" in data:
            try:
                newStatus = int(data['status'])
                if 1 <= newStatus <= 10:
                    task.status = newStatus
            except:
                logger.debug("Bad task status passed in")

        if "assignee" in data and data["assignee"] is not None:
            try:
                if "username" in data["assignee"]:
                    username = data["assignee"]["username"]
                else:
                    username = data["assignee"]

                user = User.objects.get(username=username)
                if has_read_access(project, user):
                    task.assignee = user
            except User.DoesNotExist:
                pass

        task.save()
        signals.task_created.send(sender=request, task=task, user=request.user)
        realtime_util.send_task_created(project, task)
        return task

    def update(self, request, organization_slug, project_slug, story_id, task_id, iteration_id=None):
        data = request.data
        org, project = checkOrgProject(request, organization_slug, project_slug, check_write_access=True)

        task = Task.objects.get(id=task_id)
        if task.story.project != project:
            raise ValidationError("Organization and project don't match")

        old_copy = task.__dict__.copy()

        status_changed = False
        if task.status != data['status']:
            status_changed = True

        for field in TaskHandler.write_fields:
            if field in data:
                setattr(task, field, data[field])

        if "assignee" in data:
            if data["assignee"] is None:
                task.assignee = None
            else:
                try:
                    if "username" in data["assignee"]:
                        username = data["assignee"]["username"]
                    else:
                        username = data["assignee"]
                    user = User.objects.get(username=username)
                    if has_read_access(project, user):
                        task.assignee = user
                except User.DoesNotExist:
                    task.user = None

        if "status" in data:
            try:
                newStatus = int(data['status'])
                if 1 <= newStatus <= 10:
                    task.status = newStatus
            except:
                logger.debug("Bad task status passed in")

        # There's a special task_id_before/task_id_after pair that can be set to affect reordering
        # of the story.  We do this last so the _reorderStory algorithm can make inferences from
        # the current story data.
        if ("task_before_id" in data) and ("task_after_id" in data):
            task.reorderResults = self._reorderTask(task, data, project)

        task.save()
        new_copy = task.__dict__.copy()

        if status_changed:
            kanban_manager.autoAssignUserToTask(task, request.user)
            signals.task_status_changed.send(sender=request, task=task, user=request.user)
        else:
            signals.task_updated.send(sender=request, task=task, user=request.user)

        diffs = model_differences(old_copy, new_copy, ["modified"], dicts=True)
        props = {k: v[1] for k, v in diffs.iteritems()}
        extraPatchFields = {
            'tags': lambda obj: task.tags_cache if task.tags_cache is not None else "",
            'assignee': lambda obj: self._task_assignee(obj),
            'reorderResults': lambda obj: obj.reorderResults if hasattr(obj, "reorderResults") else None
        }
        for k, v in extraPatchFields.iteritems():
            props[k] = v(task)

        realtime_util.send_task_patch(project, task, props)
        return task

    def delete(self, request, organization_slug, project_slug, story_id, task_id, iteration_id=None):
        org, project = checkOrgProject(request, organization_slug, project_slug, True)

        task = Task.objects.get(id=task_id)
        story_id = task.story_id

        if task.story.project != project:
            logger.debug("Project did not match %d %d" % (task.story.project.id, project.id))
            raise ValidationError("Organization and project don't match")

        signals.task_deleted.send(sender=request, task=task, user=request.user)
        task.sync_queue.clear()
        task.delete()
        realtime_util.send_task_deleted(project, story_id, task_id)
        return "deleted"
