from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ("id",
                  "complete",
                  "summary",
                  ("assignee", ("username", "first_name", "last_name", "id")),
                  "order",
                  "status",
                  "reorderResults",
                  "estimated_minutes")
        extra_kwargs = {
            'complete': {'write_only': True},
            'summary': {'write_only': True},
            'estimated_minutes': {'write_only': True}
        }
