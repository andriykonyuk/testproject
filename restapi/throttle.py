from rest_framework.throttling import UserRateThrottle


class WriteRateThrottle(UserRateThrottle):
    scope = 'write'


class ReadRateThrottle(UserRateThrottle):
    scope = 'read'
