from django.contrib.auth.models import User
from django.db import models
from django.core import validators


class Task(models.Model):
    summary = models.TextField(blank=True)
    assignee = models.ForeignKey(User, related_name="assigned_tasks", verbose_name='assignee', null=True, blank=True,
                                 on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)
    tags_cache = models.CharField(max_length=512, blank=True, null=True, default=None)

    estimated_minutes = models.IntegerField(default=0)

    status = models.IntegerField(default=1,
                                 validators=[validators.MinValueValidator(1), validators.MaxValueValidator(10)])

    modified = models.DateTimeField('modified', auto_now=True)

    tags_to_delete = []
    tags_to_add = []
    new_project_tags = []

    def task_tags_array(self):
        tags = self.tags.split(",")
        return [tag.strip() for tag in tags if len(tag.strip()) > 0]

    def task_tags_full(self):
        "Helper function to return queryset of taggings with the tag object preloaded"
        return self.task_tags.all().select_related("tag")

    def resetTagsCache(self):
        r = ""
        for tag in self.task_tags.all():
            if len(r) > 0:
                r = r + ", "
            r = r + tag.name
        self.tags_cache = r

    # For backwards compatibility when I added the status field:
    @property
    def complete(self):
        return self.status == 10

    @complete.setter
    def complete(self, value):
        if value:
            self.status = 10
        else:
            self.status = 1

    @property
    def tags(self):
        if self.tags_cache is None:
            self.resetTagsCache()
        return self.tags_cache

    @tags.setter
    def tags(self, value):
        # print "TAGS SET " + value
        self.tags_cache = value
        input_tags = [t.strip() for t in value.split(',')]
        self.tags_to_delete = []
        self.tags_to_add = []
        self.new_project_tags = []
        # First, find all the tags we need to add.
        for input_tag in input_tags:
            found = False
            for saved_tag in self.task_tags.all():
                if saved_tag.name == input_tag:
                    found = True
            if not found and input_tag != "":
                self.tags_to_add.append(input_tag)
            # Next, find the tags we have to delete
        for saved_tag in self.task_tags.all():
            found = False
            for input_tag in input_tags:
                if saved_tag.name == input_tag:
                    found = True
            if not found:
                self.tags_to_delete.append(saved_tag)

    def export_value(self):
        status = self.status_text()
        if self.summary == "":
            return ""
        if self.assignee == None:
            return self.summary + "\t" + status
        else:
            return self.summary + "\t" + self.assignee.username + "\t" + status

    def time_estimate_label(self):
        minutes = self.estimated_minutes

        return "%d:%02d hr" % divmod(minutes, 60)

    class Meta:
        ordering = ['order']
        app_label = 'projects'
        db_table = "v2_projects_task"
